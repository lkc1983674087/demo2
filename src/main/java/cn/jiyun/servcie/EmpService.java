package cn.jiyun.servcie;

import cn.jiyun.mapper.EmpMapper;
import cn.jiyun.pojo.Dept;
import cn.jiyun.pojo.EmpVo;
import cn.jiyun.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpService {

    @Autowired
    EmpMapper empMapper;

    public List<EmpVo> findAll(Employee employee) {
        return empMapper.findAll(employee);
    }

    public void add(Employee employee) {
        empMapper.add(employee);
    }

    public List<Dept> findDept() {

        return empMapper.findDept();
    }

    public void update(Employee employee) {
        empMapper.update(employee);
    }

    public void delete(Integer id) {
        empMapper.delete(id);
    }

    public Employee findBYId(Integer id) {
        return empMapper.findBYId(id);
    }

}
