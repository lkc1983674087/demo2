package cn.jiyun.pojo;

public class EmpVo extends Employee{

    private String ename;
    private String cname;

    @Override
    public String toString() {
        return "EmpVo{" +
                "ename='" + ename + '\'' +
                ", cname='" + cname + '\'' +
                '}';
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
}
