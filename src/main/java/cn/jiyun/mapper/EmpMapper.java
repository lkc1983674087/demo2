package cn.jiyun.mapper;


import cn.jiyun.pojo.Dept;
import cn.jiyun.pojo.EmpVo;
import cn.jiyun.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface EmpMapper {

    List<EmpVo> findAll(Employee employee);

    void add(Employee employee );


    List<Dept> findDept();

    Employee findBYId(Integer id);

    void delete(Integer id);

    void update(Employee employee);
}
