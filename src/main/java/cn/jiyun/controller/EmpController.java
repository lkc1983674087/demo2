package cn.jiyun.controller;

import cn.jiyun.pojo.Dept;
import cn.jiyun.pojo.EmpVo;
import cn.jiyun.pojo.Employee;
import cn.jiyun.servcie.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
@RequestMapping("emp")
public class EmpController {

    @Autowired
    EmpService empService;

    @RequestMapping("show")
    public String findAll(Employee employee, HttpServletRequest request) {
        List<EmpVo> list = empService.findAll(employee);
        request.setAttribute("list", list);
        return "show";
    }

    @RequestMapping("toAdd")
    public String insert(HttpServletRequest request) {
        List<Dept> deptList = empService.findDept();
        request.setAttribute("deptList", deptList);
        return "add";

    }

    @RequestMapping("add")
    public String add(Employee employee) {
        empService.add(employee);
        return "redirect:show";
    }

    @RequestMapping("toupdate")
    public String uu(Integer id, HttpServletRequest request) {
        Employee e=empService.findBYId(id);
        request.setAttribute("e", e);
        return "update";

    }

    @RequestMapping("update")
    public String update(Employee employee) {
        empService.update(employee);
        return "redirect:show";
    }

    @RequestMapping("delete")
    public String delete(Integer id) {
        empService.delete(id);
        return "redirect:show";
    }
}
